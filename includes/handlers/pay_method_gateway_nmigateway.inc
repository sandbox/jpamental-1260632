<?php // $Id: pay_method_gateway_nmigateway.inc,v 1.1 2010/11/01 20:16:52  Exp $

/**
 * @file
 */

class pay_method_gateway_nmigateway extends pay_method_gateway {
  var $nmigateway_login = '';
  var $nmigateway_key = '';
  var $nmigateway_email_customer = 0;
  var $nmigateway_email_merchant = 1;
  var $nmigateway_developer = 0;

  var $gateway_supports_ach = TRUE;
  var $gateway_supports_cc = TRUE;
  var $gateway_supports_recurring = FALSE;

  function gateway_url() {
    $url = 'https://secure.networkmerchants.com/gw/api/transact.php';
    // below is from Diamond Mind API docs, but above works and is more generic as Diamond Mind is a 'private label' service based on NMI
    //$url = 'https://pcisecure.diamondmindschools.com/api/transact.php'; 
    //if ($this->nmigateway_developer) {
    	// think this actually needs to go further down to affect the array
  	//	var $nmigateway_login = 'demo';
  	//	var $nmigateway_key = 'password';
    //}
    return $url;
  }

  function gateway_request() {
    // Set the transaction type based on requested activity.
    if (!$txntype = $this->nmigateway_trxtype($this->activity->activity)) {
      drupal_set_message("Payment activity '$this->activity->activity' is unsupported. Not processing transaction", 'error');
      return FALSE;
    }

    $req = '';
    foreach ($this->gateway_request_values($txntype) as $key => $val) {
      if ($val = trim($val)) $req .= check_plain($key) .'='. urlencode($val) .'&'; 
    }
	$req = substr($req, 0, -1); // strip trailing '&'
    return $req;
  }

  function execute($activity) {
    $this->activity = $activity;
    if ($request = $this->gateway_request()) {
      $ret = drupal_http_request($this->gateway_url(), array('Content-Type' => 'application/x-www-form-urlencoded'), 'POST', $this->gateway_request()); // headers set here to correct for POST type error in form submission
      if ($ret->error) {
        watchdog('payment', "Gateway Error: @err Payment NOT processed.", array('@err' => $ret->error));
        $this->activity->data = (array) $ret;
        return FALSE;
      }
      else {
        return $this->gateway_response($ret->data);
      }
    }
  }




  // TODO
  function gateway_response($result) {
    $resultString = $result;
    $resultArray = array();
    $resultArrayTemp = explode('&', ($result));
    foreach($resultArrayTemp as $row) {
    	$thisRow = explode('=', ($row));
    	$resultArray[$thisRow[0]] = $thisRow[1];
    }

    // Save the transaction ID for tracking and/or future operations.
    $this->activity->identifier = $resultArray['transactionid'];

    $response_code = $resultArray['response'];
  
    $this->activity->data = array(
      'response_string' => $resultArray['responsetext'],
      'approval_code'   => $resultArray['authcode'],
      'avs_response'    => $resultArray['avsresponse'],
      'caav_response'   => $resultArray['cvvresponse'],
    );

    /* Return TRUE if response_code = 1. Possible values:
      1 = Approved
      2 = Declined
      3 = Error
    */

    if ($response_code != 1) {
      watchdog('payment', "Error processing payment: NMI Gateway returned '@err'", array('@err' => $this->activity->data['response_string']));
      drupal_set_message("Error processing payment: NMI Gateway returned '". $this->activity->data['response_string']. "' (full result string: ".$resultString.")");
    }

    return ($response_code == 1);
  }

  function settings_form(&$form, &$form_state) {
    parent::settings_form($form, $form_state);
    $group = $this->handler();

    $form[$group]['an']['#type'] = 'fieldset';
    $form[$group]['an']['#collapsible'] = FALSE;
    $form[$group]['an']['#title'] = t('NMI Gateway settings');
    $form[$group]['an']['#group'] = $group;

    $form[$group]['an']['nmigateway_login'] = array(
      '#type' => 'textfield',
      '#title' => t('NMI Gateway Username'),
      '#default_value' => $this->nmigateway_login,
      '#required' => TRUE,
      '#parents' => array($group, 'nmigateway_login'),
    );
    $form[$group]['an']['nmigateway_key'] = array(
      '#type' => 'textfield',
      '#title' => t('NMI Gateway Password'),
      '#default_value' => $this->nmigateway_key,
      '#required' => TRUE,
      '#parents' => array($group, 'nmigateway_key'),
    );
  }

  function nmigateway_trxtype($activity) {
    $trxtypes = array(
      'complete'  => 'sale',
      'authorize' => 'auth',
    );
    return $trxtypes[$activity];
  }

  // TODO
  function gateway_request_values($txntype) {
    $data = array(
		'username' => $this->nmigateway_login,
		'password' => $this->nmigateway_key,

		// Customer Name and Billing Address
		'firstname' => $this->first_name,
		'lastname' => $this->last_name,
		'address1' => $this->billto['street1'],
		'address2' => $this->billto['street2'],
		'city' => $this->billto['city'],
		'state' => $this->billto['state'],
		'zip' => $this->billto['zip'],
		'country' => $this->billto['country'],
		'phone' => $this->billto['phone'],
		'orderdescription' => $this->trx_identifier, // transaction identifier

		// Additioanl customer data
		'ipaddress' => ip_address(),

		// customer email address
		'email' => $this->mail,

		// Order information
		'orderid' => $this->activity->pxid,	
		//'orderdescription' => $this->note,

		// Transaction Data
		'amount' => $this->total(),
		'type' => $txntype,

		'ccnumber' => $this->cc_number,
		'ccexp' => $this->cc_expiration(),
		'cvv' => $this->cc_ccv2,
    );


    return $data;
  }
}
